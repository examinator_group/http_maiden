package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)


type TestDataList struct {
    name string 
    author string 
    questions []TestDataItem
}

type TestDataItem struct {
    body string
    score int
    answers []string
}

var test = TestDataList {
    name: "first test",
    author: "dso",
    questions: []TestDataItem {
        {
            body: "question 1",  
            score: 1,
            answers: []string {
                "1", "2", "3", "4",
            },
        },
        {
            body: "question 2",  
            score: 2,
            answers: []string {
                "2", "2", "2", "2",
            },
        },
    },
}

func getTest(w http.ResponseWriter, r *http.Request) {
    
    resp, err := json.Marshal(test)

    if err != nil {
        fmt.Println(err)
        return
    }

    io.WriteString(w, string(resp))
}

func main() {

    http.HandleFunc("/", getTest)

    err := http.ListenAndServe(":3333", nil)
    if err != nil {
        fmt.Printf("err: %v\n", err)
        os.Exit(1)
    }

}
